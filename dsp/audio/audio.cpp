/*
 * audio.cpp
 *
 *  Created on: Feb 18, 2021
 *      Author: RajaS
 */


#include <iostream>
#include <string>
#include <fstream>

#include <sndfile.h>

const std::string origfilename("558906.wav") ;
const std::string ch1filename("ch1.csv") ;
const std::string ch2filename("ch2.csv") ;

#define	BLOCK_SIZE 4096

void convert_to_text (SNDFILE * infile, int channels)
{	float buf [BLOCK_SIZE] ;
	sf_count_t frames ;
	int k, m, readcount ;
	unsigned long long totalcount =0;
	frames = BLOCK_SIZE / channels ;
	std::ofstream ch1file(ch1filename) ;
	std::ofstream ch2file(ch2filename) ;

	while ((readcount = sf_readf_float (infile, buf, frames)) > 0)
	{	totalcount += readcount ;
	    for (k = 0 ; k < readcount ; k++)
		{
		    ch1file << buf[k*channels] << std::endl ;
		    ch2file << buf[k*channels+1] << std::endl ;
		}
	}
	ch1file.close() ;
	ch2file.close() ;
	std::cout << "Frames read " << totalcount << std::endl ;
	return ;
}
int main(int argc, char** argv)
{


	SNDFILE		*infile = NULL ;
	SF_INFO		sfinfo ;
	if ((infile = sf_open (argv[1] , SFM_READ, &sfinfo)) == NULL)
	{	std::cout << "Not able to open input file " << argv[1] << std::endl ;
		std::cout << (sf_strerror (NULL)) << std::endl ;
		return 1 ;
	}
	std::cout << "Channels " << sfinfo.channels << std::endl ;
	std::cout << "Format " << sfinfo.format << std::endl ;
	printf("Format %x\n", sfinfo.format);
	std::cout << "Frames " << sfinfo.frames << std::endl ;
	std::cout << "Samplerate " << sfinfo.samplerate << std::endl ;
	std::cout << "Sections " << sfinfo.sections << std::endl ;
	std::cout << "Seekable " << sfinfo.seekable << std::endl ;

	float audiolen = (double)sfinfo.frames / (double) sfinfo.samplerate ;
	std::cout << "Audio len " << audiolen << std::endl ;

	convert_to_text(infile,sfinfo.channels) ;
	sf_close (infile) ;
	return 0 ;
}

