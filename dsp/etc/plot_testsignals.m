wave1=csvread("unitpulse.csv");
x=wave1(:,1);
y=wave1(:,2);
stem(x,y)
title("Unit Pulse Wave")
grid on
figure
wave2=csvread("unitstep.csv");
x2=wave2(:,1);
y2=wave2(:,2);
stem(x2,y2)
title("Unit Step Wave")