/*
 * audiowave.cpp
 *
 *  Created on: Feb 20, 2021
 *      Author: RajaS
 */

#include <iostream>
#include <string.h>
#include <sndfile.h>
#include "GAudio.h"

GAudio::GAudio()
: ch1(nullptr) , ch2(nullptr) , debug(false)
{
	memset(&sfinfo,0,sizeof(sfinfo));
}

GAudio::~GAudio()
{
    if (ch1 != nullptr) delete ch1 ;
	if (ch2 != nullptr) delete ch2 ;
}

double GAudio::Duration()
{
	if (sfinfo.frames > 0)
	{
		double dur = (double)sfinfo.frames / (double) sfinfo.samplerate ;
		return dur ;
	}
	return 0.0;
}
const int BLOCK_SIZE = 4096 ;

void GAudio::Load(std::string _filename) throw()
{
	SNDFILE		*infile = NULL ;

	if ((infile = sf_open (_filename.c_str() , SFM_READ, &sfinfo)) == NULL)
	{	if (debug)
		{
			std::cout << "Not able to open input file " << _filename.c_str() << std::endl ;
			std::cout << (sf_strerror (NULL)) << std::endl ;

		}
		throw std::unexpected ;
	}
	if (debug)
	{
		std::cout << "Channels " << sfinfo.channels << std::endl ;
		std::cout << "Format " << sfinfo.format << std::endl ;
		std::cout << "Frames " << sfinfo.frames << std::endl ;
		std::cout << "Samplerate " << sfinfo.samplerate << std::endl ;
		std::cout << "Sections " << sfinfo.sections << std::endl ;
		std::cout << "Seekable " << sfinfo.seekable << std::endl ;
	}

	double dur = (double)sfinfo.frames / (double) sfinfo.samplerate ;
	if (debug)
	{
		std::cout << "Audio duration " << dur << std::endl ;
	}

	ch1 = new GWave( sfinfo.samplerate, 0.0 , dur) ;
	if (sfinfo.channels > 1) 
	{
		ch2 = new GWave( sfinfo.samplerate, 0.0 , dur) ;
	}

	float buf [BLOCK_SIZE] ;
	sf_count_t frames ;
	int k, m, readcount ;
	unsigned long long totalcount =0;
	frames = BLOCK_SIZE / sfinfo.channels ;
	while ((readcount = sf_readf_float (infile, buf, frames)) > 0)
	{
		for (k = 0 ; k < readcount ; k++, totalcount++)
		{
			ch1->Set(totalcount,buf[k*sfinfo.channels]);
			if (sfinfo.channels > 1)
			{
				ch2->Set(totalcount,buf[k*sfinfo.channels+1]);
			}
		}
	}
	sf_close (infile) ;
	if (debug) std::cout << "Frames read " << totalcount << std::endl ;

}

void GAudio::Save(std::string _filename, double _start , double _duration ) throw()
{
	SNDFILE		*outfile = NULL ;
	SF_INFO save_sfinfo ;
	save_sfinfo = sfinfo ;
	save_sfinfo.channels = 2 ;
	save_sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT ;
	save_sfinfo.samplerate = ch1->Framerate() ;

	if (_duration < 0.0) save_sfinfo.frames = ch1->Count() ;
	else                 save_sfinfo.frames = _duration * ch1->Framerate() ;

	save_sfinfo.sections = 1 ;
	save_sfinfo.seekable = 1 ;
	if (debug)
	{
		std::cout << "Creating output file " << _filename << std::endl ;
		std::cout << "Frames " << save_sfinfo.frames << " Frame rate " << ch1->Framerate() << std::endl ;
	}
	int framestowrite = save_sfinfo.frames ;
	if (! (outfile = sf_open (_filename.c_str() , SFM_WRITE, &save_sfinfo)))
	{
		if (debug)
		{
			printf ("Error : could not open file : %s\n", _filename.c_str()) ;
			puts (sf_strerror (NULL)) ;
		}
		throw std::unexpected ;
	} ;

	float buffer [BLOCK_SIZE] ;
	sf_count_t frames ;
	int writecount =0;

	frames = BLOCK_SIZE / save_sfinfo.channels ;

	int s ;

	int fromidx = ch1->Index(_start);
	if (debug) std::cout << "Fromidx " << fromidx << " Max count " << ch1->Count() << " frames " << framestowrite << " " << writecount << std::endl ;
	while ( writecount < framestowrite )
	{
		int f ;
		for (f=0; (f < frames) && (fromidx < ch1->Count() - 1)  && (writecount < framestowrite) ; f++)
		{
			buffer[f*2] = ch1->Value(fromidx);
			buffer[f*2+1] = ch2->Value(fromidx);
			fromidx++ ;
			writecount++ ;
		}
		sf_writef_float (outfile, buffer, f ) ;
	}
	sf_close (outfile) ;
	if (debug) std::cout << "Frames written " << writecount << std::endl ;

}