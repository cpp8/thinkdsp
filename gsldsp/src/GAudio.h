/// \brief Audio File support
///
/// GAudio class supports loading and saving audio files in .wav format. This should support
/// experimentation with signal processing algorithms.
///

#include <string>
#include <algorithm>
#include <sndfile.h>
#include "GWave.h"

class GAudio
{
public:
    GAudio() ;
    virtual ~GAudio() ;
    double Duration() ;
    void Load(std::string _filename) throw();
    void Save(std::string _filename, double _start=0.0, double _duration=-1.0) throw();
    std::pair<GWave*, GWave*> Samples() { return std::make_pair(ch1,ch2) ; }
private:
    bool debug ;
    GWave *ch1 ;
    GWave *ch2 ;
    SF_INFO sfinfo ;
} ;

