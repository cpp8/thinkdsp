#include <gsl/gsl_rng.h>
#include "GNoise.h"

GBrownian::GBrownian(unsigned long long _framerate, double _duration) 
:GWave(_framerate,0.0,_duration)
{
    
}

GBrownian::~GBrownian()
{

}

void GBrownian::Generate(double _amplitude) 
{
    //GWave result(_framerate,0.0,_duration);
    gsl_rng * r = gsl_rng_alloc (gsl_rng_knuthran2002);
    gsl_rng_set(r, 314159265 );
    double prev=0.0 ;
    for (unsigned long long i=0; i < Count() ; i++)
    {
        double noise=gsl_rng_uniform(r) ;
        noise = prev + _amplitude * (1.0 - 2. * noise);

        if (noise > _amplitude)
        {
            noise -= _amplitude ;
        }
        else if (noise < -_amplitude)
             {
                 noise += _amplitude ;
             }
        gsl_vector_set( values , i , noise ) ;
        prev = noise ;
    }
    gsl_rng_free(r);
}