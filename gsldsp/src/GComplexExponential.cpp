/*
 * GGComplexExponential.cpp
 *
 *  Created on: Feb 24, 2021
 *      Author: RajaS
 */
#include <cmath>
#include "GComplexExponential.h"

GComplexExponential::GComplexExponential(unsigned long long _frequency, double _amplitude, double _phase)
: GComplexSignal(_frequency,_amplitude,_phase)
{

}

GComplexExponential::~GComplexExponential() {
	// TODO Auto-generated destructor stub
}

GComplexWave GComplexExponential::Generate(unsigned long long _framerate , double _start, double _duration)
{
    GComplexWave result(_framerate,_start,_duration) ;
    double delta = result.Period() ;
    int count = result.Count() ; 
    double x = result.Start() ;
    gsl_complex sample ;

    for (int i=0; i<count; i++)
    {
    	double multiplier = pow(fabs(amplitude),x) ;
    	double im = multiplier * sin(2.0*M_PI*frequency*x) ;
    	double re = multiplier * cos(2.0*M_PI*frequency*x) ;
        sample.dat[0] = re ;
        sample.dat[1] = im ;

    	result.Set(x,sample);
    	//cout << "Setting at " << x << " value " << val << endl ;
    	x += delta ;
    }
    return result ;
}
