/*
 * GComplexExponential.h
 *
 *  Created on: Feb 24, 2021
 *      Author: RajaS
 */

#ifndef GComplexExponential_H_
#define GComplexExponential_H_

#include "GComplexSignal.h"

class GComplexExponential: public GComplexSignal {
public:
	GComplexExponential(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GComplexExponential();
	virtual GComplexWave Generate(unsigned long long _framerate , double _start=0.0, double _duration=1.0) ;
};

#endif /* GComplexExponential_H_ */
