/*
 * GComplexSignal.cpp
 *
 *  Created on: Feb 23, 2021
 *      Author: RajaS
 */

#include "GComplexSignal.h"

GComplexSignal::GComplexSignal(unsigned long long _frequency, double _amplitude, double _phase)
: frequency(_frequency) , amplitude(_amplitude) , phase(_phase)
{

}

GComplexSignal::~GComplexSignal() {

}

