/*
 * GComplexSignal.h
 *
 *  Created on: Feb 23, 2021
 *      Author: RajaS
 */

#ifndef GComplexSignal_H_
#define GComplexSignal_H_
#include <gsl/gsl_math.h>
#include "GComplexWave.h"

class GComplexSignal {
public:
	GComplexSignal(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GComplexSignal();

	virtual GComplexWave Generate(unsigned long long _framerate , double _start, double _duration) = 0 ;
protected:
	unsigned long long frequency ;
	double amplitude ;
	double phase ;
};

#endif /* GComplexSignal_H_ */
