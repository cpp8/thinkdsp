/*
 * GComplexSinusoid.cpp
 *
 *  Created on: Feb 23, 2021
 *      Author: RajaS
 */
#define _USE_MATH_DEFINES
#include <cmath>
#include "GComplexSinusoid.h"

GComplexSinusoid::GComplexSinusoid(unsigned long long _frequency, double _amplitude, double _phase)
: GComplexSignal(_frequency,_amplitude,_phase)
{
}

GComplexSinusoid::~GComplexSinusoid()
{
}

GComplexWave GComplexSinusoid::Generate(unsigned long long _framerate , double _start, double _duration)
{
    GComplexWave result(_framerate,_start,_duration) ;
    double delta = result.Period() ;
    int count = result.Count() ;
    double x = result.Start() ;
    gsl_complex sample ;
    for (int i=0; i<count; i++)
    {
    	double im = amplitude * sin(2.0*M_PI*frequency*x + phase) ;
    	double re = amplitude * cos(2.0*M_PI*frequency*x + phase) ;
        sample.dat[0] = im ;
        sample.dat[1] = re ;
    	result.Set(x,sample);
    	//cout << "Setting at " << x << " value " << val << endl ;
    	x += delta ;
    }
    return result ;
}
