/*
 * GComplexSinusoid.h
 *
 *  Created on: Feb 23, 2021
 *      Author: RajaS
 */

#ifndef GComplexSinusoid_H_
#define GComplexSinusoid_H_

#include "GComplexSignal.h"

class GComplexSinusoid: public GComplexSignal {
public:
	GComplexSinusoid(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GComplexSinusoid();
	virtual GComplexWave Generate(unsigned long long _framerate , double _start=0.0, double _duration=1.0) ;
};

#endif /* GComplexSinusoid_H_ */
