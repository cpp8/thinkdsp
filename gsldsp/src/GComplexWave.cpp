/*
 * GComplexWave.cpp
 *
 *  Created on: Feb 23, 2021
 *      Author: RajaS
 */
#include <iostream>
#include <fstream>
#include "GComplexWave.h"
using namespace std ;
GComplexWave::GComplexWave(unsigned long long _framerate , double _start , double _duration )
: framerate(_framerate) , start(_start) , duration(_duration)
{
    int count = _duration * _framerate ;
    values = gsl_vector_complex_calloc(count) ;
	period = 1.0/(double)framerate ;
}

GComplexWave::~GComplexWave()
{
    if (values) gsl_vector_complex_free(values);
}


int GComplexWave::Index(double _time)
{
	int idx = floor((_time - start) /period) ;
	return idx ;
}

double GComplexWave::ApproxIndex(double _time)
{
	double idx = (_time - start) / period ;
	return idx ;
}

void GComplexWave::Set(double _time, gsl_complex _value)
{
	int idx = Index(_time) ;
    gsl_vector_complex_set(values,idx,_value) ;
}

void GComplexWave::Show(std::string _filename) const
{
	std::ofstream csvfile(_filename) ;
    double t = start ;
    for (int i=0; i < values->size; i++)
    {
        gsl_complex v = gsl_vector_complex_get(values, i) ;
        csvfile << t << "," << v.dat[0] << "," << v.dat[1] << endl ;
        t += period ;       
    }
    csvfile.close() ;
}

