
/*
 * GComplexWave.h
 *
 *  Created on: Feb 23, 2021
 *      Author: RajaS
 */

#ifndef GComplexWave_H_
#define GComplexWave_H_
#include "GWave.h"

class GComplexWave
{
public:
	GComplexWave(unsigned long long _framerate , double _start , double _duration ) ;
	virtual ~GComplexWave() ;

	void Set(double _time, gsl_complex _value) ;
    gsl_complex Value(double _time) ;

	int Index(double _time) ;
	double ApproxIndex(double _time) ;

	int Count() { return duration * framerate ; }
	double Start() { return start ; }
	double Period() { return period ; }

	void Show(std::string _filename) const ;

	GWave Real() const ;
	GWave Imag() const ;

private:
	
	unsigned long long framerate ;
	double start;
	double duration;
	double period ;
    gsl_vector_complex *values ;
};

#endif /* GComplexWave_H_ */
