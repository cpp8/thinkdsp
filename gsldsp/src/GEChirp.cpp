#include "GEChirp.h"

GEChirp::GEChirp(unsigned long long _framerate, double _start, double _duration)
: GWave(_framerate,_start,_duration)
{

}

GEChirp::~GEChirp()
{

}

void GEChirp::Generate(double _amplitude, double _fstart , double _fend , double _initphase)
{
    double chirpiness = pow(_fend/_fstart,1.0/duration) ;
    double lnchirpiness = log(chirpiness) ;

    for (int i=0; i < values->size; i++)
    {
        double time = start + i * period ;
        double phase = _initphase + 2.0 * M_PI * _fstart * ( (pow(chirpiness,time) -1.0) / lnchirpiness ) ;
        double val = _amplitude * sin( phase ) ;
        gsl_vector_set(values,i,val) ;
    }
}
