#ifndef GECHIRP_H_
#define GECHIRP_H_
#include "GWave.h"

class GEChirp : public GWave 
{
public:
    GEChirp(unsigned long long _framerate, double _start, double _duration);
    ~GEChirp() ;
    void Generate(double _amplitude, double _fstart , double _fend , double _initphase=0.0);
};
#endif