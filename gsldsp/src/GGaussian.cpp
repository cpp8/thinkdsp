#include <gsl/gsl_randist.h>
#include "GNoise.h"

GGaussian::GGaussian(unsigned long long _framerate, double _duration)
: GWave(_framerate,0.0,_duration)
{

}

GGaussian::~GGaussian()
{

}

void GGaussian::Generate(double _mean, double _stdev, double _amplitude)
{
    gsl_rng * r = gsl_rng_alloc (gsl_rng_knuthran2002);
    gsl_rng_set(r, 314159265 );
    for (unsigned long long i=0; i < Count() ; i++)
    {
        double noise=gsl_ran_gaussian(r, _stdev) + _mean ;
        gsl_vector_set( values , i , _amplitude * noise ) ;
    }
    gsl_rng_free(r);
}