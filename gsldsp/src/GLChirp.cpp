#include "GLChirp.h"

GLChirp::GLChirp(unsigned long long _framerate, double _start, double _duration)
: GWave(_framerate,_start,_duration)
{

}

GLChirp::~GLChirp()
{

}

void GLChirp::Generate(double _amplitude, double _fstart , double _fend , double _initphase)
{
    double chirpiness = (_fend - _fstart) / duration ;
    for (int i=0; i < values->size; i++)
    {
        double time = start + i * period ;
        double phase = _initphase + 2.0 * M_PI * time *( _fstart + chirpiness * time / 2.0 ) ;
        double val = _amplitude * sin( phase ) ;
        gsl_vector_set(values,i,val) ;
    }
}
