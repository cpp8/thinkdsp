#ifndef GLCHIRP_H_
#define GLCHIRP_H_
#include "GWave.h"

class GLChirp : public GWave 
{
public:
    GLChirp(unsigned long long _framerate, double _start, double _duration);
    ~GLChirp() ;
    void Generate(double _amplitude, double _fstart , double _fend , double _initphase=0.0);
};
#endif