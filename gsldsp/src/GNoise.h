#ifndef GNOISE_H_
#define GNOISE_H_
#include "GWave.h"


class GUUNoise : public GWave
{
public:
    GUUNoise(unsigned long long _framerate, double _duration=1.0);
    virtual ~GUUNoise() ;
    void Generate(double _amplitude=1.0);
};

class GGaussian : public GWave
{
public:
    GGaussian(unsigned long long _framerate, double _duration=1.0);
    virtual ~GGaussian() ;
    void Generate(double _mean=0.0, double _stdev=1.0, double _amplitude=1.0) ;
};

class GBrownian : public GWave
{
public:
    GBrownian(unsigned long long _framerate, double _duration=1.0);
    virtual ~GBrownian() ;
    void Generate(double _amplitude=1.0) ;
} ;


#endif