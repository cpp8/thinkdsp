/*
 * GSignal.h
 *
 *  Created on: Mar 3, 2021
 *      Author: RajaS
 */

/// \brief Periodic Signal
///
/// The GSignal class represents a periodic signal which has a frequency, an amplitude and
/// a phase. In theory, the signal from negative infinity to positive infinity of time units
///
/// Different specific signals provide an implementation to generate a GWave which in turn are
/// discrete samples for a subset of the time scale.

#ifndef GSLDSP_SRC_GSIGNAL_H_
#define GSLDSP_SRC_GSIGNAL_H_
#include "GWave.h"
class GSignal {
public:
	GSignal() ;
	GSignal(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GSignal();
	virtual GWave Generate(unsigned long long _frequency, double _start, double _duration) = 0 ;
protected:
	unsigned long long frequency ;
	double amplitude ;
	double phase ;
};

#endif /* GSLDSP_SRC_GSIGNAL_H_ */
