#define _USE_MATH_DEFINES
#include <cmath>

#include "GSinc.h"

GSinc::GSinc()
{

}

GSinc::GSinc(unsigned long long _frequency, double _amplitude, double _phase)
: GSignal(_frequency,_amplitude,_phase)
{

}

GSinc::~GSinc()
{

}


GWave GSinc::Generate(unsigned long long _framerate , double _start, double _duration)
{
    GWave result(_framerate,_start,_duration );
    unsigned long long samples = _duration * _framerate ;
    double period = result.Period() ;
    double time = result.Start() ;
    double value ;
    for (unsigned long long i=0; i < samples; i++)
    {
        if (fabs(time+phase) < EPSILON)
        {
            value = amplitude;
        }
        else
        {
            value = amplitude * sin(time+phase) / (time+phase) ;
        }
        result.Set( time , value );
        time += period ;
    }
    return result ;
}
