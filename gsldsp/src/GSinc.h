#ifndef _GSLDSP_SRC_GSINC_H_
#define _GSLDSP_SRC_GSINC_H_

#include "GSignal.h"

class GSinc : public GSignal
{
protected:
	GSinc() ;
public:
	GSinc(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GSinc();
	virtual GWave Generate(unsigned long long _framerate, double _start, double _duration) ;
private:
    const double EPSILON = 0.00001 ;
} ;
#endif