#define _USE_MATH_DEFINES
#include <cmath>
#include "GSinusoid.h"

GSinusoid::GSinusoid()
{

}

GSinusoid::GSinusoid(unsigned long long _frequency, double _amplitude, double _phase)
: GSignal(_frequency,_amplitude,_phase)
{

}

GSinusoid::~GSinusoid()
{

}

GWave GSinusoid::Generate(unsigned long long _framerate , double _start, double _duration)
{
    GWave result(_framerate,_start,_duration );
    unsigned long long samples = _duration * _framerate ;
    double period = result.Period() ;
    double time = result.Start() ;
    
    for (unsigned long long i=0; i < samples; i++)
    {
        double value = amplitude * sin( 2.0 * M_PI * frequency * time + phase );
        result.Set( time , value );
        time += period ;
    }
    return result ;
}
