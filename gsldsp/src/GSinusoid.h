/*
 * GSignal.h
 *
 *  Created on: Mar 3, 2021
 *      Author: RajaS
 */

#ifndef GSLDSP_SRC_GSINUSOID_H_
#define GSLDSP_SRC_GSINUSOID_H_

#include "GSignal.h"

class GSinusoid : public GSignal
{
protected:
	GSinusoid() ;
public:
	GSinusoid(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GSinusoid();
	virtual GWave Generate(unsigned long long _framerate, double _start=0.0, double _duration = 2 * M_PI ) ;
} ;

#endif
