#include <fstream>
#include <cmath>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

#include "GSpectrum.h"

GSpectrum::GSpectrum()
{

}

GSpectrum::~GSpectrum()
{
    if (real) gsl_vector_free(real);
    if (complex) gsl_vector_complex_free( complex);
}

GSpectrum::GSpectrum(unsigned long long _framerate, bool _full)
: framerate(_framerate) , full(_full), real(NULL), complex(NULL)
{

}

gsl_vector* GSpectrum::Power() 
{
    gsl_vector* result ;
    result = gsl_vector_alloc(complex->size) ;

    for (int i=0; i<result->size; i++)
    {
        gsl_complex cfreq = gsl_vector_complex_get(complex,i);
        gsl_complex_abs(cfreq) ;
        gsl_vector_set(result,i,pow(gsl_complex_abs(cfreq),2.0)) ;
    }
    return result ;
}

#if 0


double GSpectrum::MaxDiff(GSpectrum& _other) 
{
    double result=0.0;
    if (framerate != _other.framerate) 
    {
    }
    else
    {
        gsl_error("Incompatible waves for Addition", __FILE__ , __LINE__ , GSL_EINVAL) ;
        return result ;
    }

    if (real->size != _other.real->size) 
    {
    }
    else
    { 
        gsl_error("Incompatible waves for Addition", __FILE__ , __LINE__ , GSL_EINVAL) ;
        return result ;
    }

    for (int i=0; i < real->size; i++)
    {
        double temp=fabs(gsl_vector_get(real,i) - gsl_vector_get(_other.real,i)) ;
        if (result < temp)
        {
            result = temp ;
        }
    }
    return result ;
}
#endif

void GSpectrum::LowPass(unsigned long long _freqhigh) 
{
    gsl_complex zero;
    zero.dat[0] = 0.0;
    zero.dat[1] = 0.0;

    for (unsigned long long f=_freqhigh; f < complex->size; f++)
    {
        gsl_vector_complex_set(complex,f,zero) ;
        gsl_vector_set(real,f,0.0) ;
    }
}
void GSpectrum::BandPass(unsigned long long _from, unsigned long long  _to) 
{
    gsl_complex zero;
    zero.dat[0] = 0.0;
    zero.dat[1] = 0.0;
    for (unsigned long long f=_from; f < complex->size && f < _to ; f++)
    {
        gsl_vector_complex_set(complex,f,zero) ;
        gsl_vector_set(real,f,0.0) ;
    }
}
void GSpectrum::HighPass(unsigned long long _freqlow) 
{
    gsl_complex zero;
    zero.dat[0] = 0.0;
    zero.dat[1] = 0.0;

    for (unsigned long long f=0; f < complex->size && f < _freqlow ; f++)
    {
        gsl_vector_complex_set(complex,f,zero) ;
        gsl_vector_set(real,f,0.0) ;
    }
}

void GSpectrum::Show(std::string _filename) 
{
	std::ofstream csvfile(_filename) ;
    for (int i=1; i < complex->size ; i++)
	{
        gsl_complex cfreq ;
        cfreq = gsl_vector_complex_get(complex,i) ;
        csvfile << i << " , " ;
		csvfile << gsl_complex_abs(cfreq) ;
        csvfile << std::endl ;        
	}
    csvfile.close() ;
}
void GSpectrum::ShowPower(std::string _filename, gsl_vector *_power)
{
    if (_power == NULL)
    {
        _power = Power() ;
    }
    std::ofstream csvfile(_filename) ;
    for (int i=1; i < complex->size ; i++)
	{
        csvfile << i << " , " ;
		csvfile << gsl_vector_get(_power,i) ;
        csvfile << std::endl ;        
	}
    csvfile.close() ;
}