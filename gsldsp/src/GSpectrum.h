#ifndef GSPECTRUM_H_
#define GSPECTRUM_H_
#define _USE_MATH_DEFINES
#include <cmath>
#include <gsl/gsl_vector.h>
#include <string>
#include <iostream>

class GWave ;

/// \brief GSpectrum represents the waveform in the frequency domain.
///
/// Given a GWave - a periodic signal, performing a discrete fourier transform results in
/// the spectrum. In reverse the GSpectrum can be used to construct the discrete samples of
/// the periodic signal.
///
/// Some simple operations on the spectrum include LowPass, HighPass and BandPass filters
/// which eliminate some frequency components.

class GSpectrum {
public:
    GSpectrum() ;
    ~GSpectrum() ;
    GSpectrum(unsigned long long _framerate, bool _full=false);
    GSpectrum(GWave &_wave) ;
    gsl_vector* Power() ;

    void LowPass(unsigned long long _freqhigh) ;
    void BandPass(unsigned long long _from, unsigned long long _to) ;
    void HighPass(unsigned long long _freqlow) ;

	void Show(std::string _filename) ;
    void ShowPower(std::string _filename, gsl_vector *_power=NULL);
private:
    const double EPSILON=0.01 ;
    unsigned long long framerate ;
    double duration ;
    bool full ;
    gsl_vector *real ;
    gsl_vector_complex *complex ;
    friend class GWave ;

} ;

#endif