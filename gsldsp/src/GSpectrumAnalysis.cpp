#include <cmath>
#include <iostream>
#include <fstream>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics_double.h>

#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>

#include "GWave.h"
#include "GSpectrum.h"

GSpectrum::GSpectrum(GWave &_wave)
{
    std::cout << "Wave size " << _wave.values->size << " framerate " << _wave.framerate << " duration " << _wave.duration << " vector size " << _wave.values->size << std::endl;
    framerate = _wave.framerate ;
    duration = _wave.duration ;
    complex=NULL;
    gsl_fft_real_wavetable *realwt;
    realwt = gsl_fft_real_wavetable_alloc (_wave.values->size);

    gsl_fft_real_workspace *work;   
    work = gsl_fft_real_workspace_alloc (_wave.values->size);    

    real = gsl_vector_alloc (_wave.values->size);

    gsl_vector_memcpy(real,_wave.values) ;
    std::cout << "Copied data array of size " << _wave.values->size << std::endl ;
    gsl_fft_real_transform (real->data , 1 , _wave.values->size , realwt , work);

    gsl_fft_real_wavetable_free(realwt);
    gsl_fft_real_workspace_free (work);

    complex = gsl_vector_complex_alloc(_wave.values->size) ;
    gsl_fft_halfcomplex_unpack(real->data, complex->data, _wave.values->stride, _wave.values->size );

} 