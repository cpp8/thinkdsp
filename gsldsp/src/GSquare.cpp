#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
using namespace std ;

#include "GSquare.h"

GSquare::GSquare()
{

}

GSquare::GSquare(unsigned long long _frequency, double _amplitude, double _phase)
: GSignal(_frequency,_amplitude,_phase)
{

}

GSquare::~GSquare()
{

}


GWave GSquare::Generate(unsigned long long _framerate , double _start, double _duration)
{
    GWave result(_framerate ,_start,_duration) ;
    double delta = 1.0 / _framerate ;

    int count = _duration * _framerate ;
    double x = _start ;
    double y ;
    double sigperiod = 1.0 / ((double)frequency * 2);

    cout << "Phase " << phase << "Sigperiod" << sigperiod << endl ;
    for (int i=0; i<count; i++)
    {
    	int sigcycle = (x + phase) / sigperiod ;

		if ((sigcycle %2) == 0)
		{
			y = amplitude ;
		}
		else
		{
			y = - amplitude ;
		}
    	result.Set(x,y) ;
    	x += delta ;
    }

    return result ;
}
  