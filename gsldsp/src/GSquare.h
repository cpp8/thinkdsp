#ifndef _GSLDSP_SRC_GSquare_H_
#define _GSLDSP_SRC_GSquare_H_

#include "GSignal.h"

class GSquare: public GSignal
{
protected:
	GSquare() ;
public:
	GSquare(unsigned long long _frequency, double _amplitude=1.0, double _phase=0.0);
	virtual ~GSquare();
	virtual GWave Generate(unsigned long long _framerate, double _start, double _duration) ;
} ;
#endif