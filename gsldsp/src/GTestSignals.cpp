/*
 * GTestSignals.cpp
 *
 *  Created on: Mar 4, 2021
 *      Author: RajaS
 */

#include "GTestSignals.h"

GTestSignals::GTestSignals() {
	// TODO Auto-generated constructor stub

}

GTestSignals::~GTestSignals() {
	// TODO Auto-generated destructor stub
}


GWave GTestSignals::Delta(unsigned long long _framerate, double _start, double _duration, double _amplitude, double _shift)
{
	GWave result (_framerate,_start,_duration) ;
	int idx = result.Index(_start + _shift) ;
	result.Set(_start+_shift,_amplitude) ;
	return result ;
}

GWave GTestSignals::Step(unsigned long long _framerate, double _amplitude, double _start, double _duration,  double _shift )
{
	GWave result( _framerate , _start , _duration ) ;
	int idx = result.Index(_shift) ;
	for (int i=idx; i < result.Count() ; i++)
	{
		result.Set( result.Period() * i , _amplitude );
	}
	return result ;
}

GWave GTestSignals::Pulse(unsigned long long _framerate, double _amplitude, double _start , double _duration, double _pulsestart, double _pulsewidth)
{
	GWave result( _framerate , _start , _duration ) ;
	int idx = result.Index(_pulsestart) ;
	int count = _pulsewidth * _framerate ;
	for (int i=idx; i < result.Count() && count > 0 ; i++, count--)
	{
		result.Set( result.Period() * i , _amplitude) ;
	}
	return result ;
}
