/*
 * GTestSignals.h
 *
 *  Created on: Mar 4, 2021
 *      Author: RajaS
 */

#ifndef SRC_GTESTSIGNALS_H_
#define SRC_GTESTSIGNALS_H_

#include "GSignal.h"

class GTestSignals: public GSignal {
public:
	GTestSignals();
	virtual ~GTestSignals();
	static GWave Delta(unsigned long long _framerate, double _start=0.0, double _duration=1.0, double _amplitude=1.0, double _shift=0.0) ;
	static GWave Step(unsigned long long _framerate, double _amplitude=1.0,  double _start = 0.0 , double _duration=1.0, double _shift = 0.0) ;
	static GWave Pulse(unsigned long long _framerate, double _amplitude=1.0, double _start=0.0 , double _duration=1.0 , double _pulsestart = 0.0, double _pulsewidth = 1.0) ;
};

#endif /* SRC_GTESTSIGNALS_H_ */
