/*
 * Triangle.cpp
 *
 *  Created on: Feb 14, 2021
 *      Author: RajaS
 */
#include <cmath>
#include "GTriangle.h"

GTriangle::GTriangle(unsigned long long _frequency , double _amplitude , double _phase )
: GSignal(_frequency, _amplitude, _phase)
{
}

GTriangle::~GTriangle()
{
}

GWave GTriangle::Generate(unsigned long long _framerate , double _start , double _duration )
{
	GWave result (_framerate, _start, _duration);
	int count = result.Count() ;
	double x = result.Start() ;

	double xdelta = result.Period() ;
	double ydelta = amplitude * 4.0 * (double)frequency /(double)_framerate ;
	double y = 0.0 ;
	double mult = 1.0 ;


	for (unsigned long long i=0; i < count; i++)
	{
		result.Set(i , y) ;
		x += xdelta ;
		y += ydelta * mult ;
		if ((y + ydelta * mult) > amplitude)
		{
			mult = -1.0 ;
		}
		else
		{
			if ((y + ydelta*mult) < -amplitude)
			{
			   mult = 1.0 ;
			}
		}
	}
	return result ;
}
