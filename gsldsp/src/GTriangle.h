/*
 * Triangle.h
 *
 *  Created on: Feb 14, 2021
 *      Author: RajaS
 */

#ifndef GTRIANGLE_H_
#define GTRIANGLE_H_
#include "GSignal.h"
class GTriangle : public GSignal 
{
public:
	GTriangle(unsigned long long _frequency , double _amplitude=1.0, double _phase=0.0);
	virtual ~GTriangle();
	virtual GWave Generate(unsigned long long _framerate , double _start = 0.0 , double _duration=1.0) ;
};

#endif /* TRIANGLE_H_ */
