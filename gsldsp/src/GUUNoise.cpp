#include <gsl/gsl_rng.h>

#include "GNoise.h"

GUUNoise::GUUNoise(unsigned long long _framerate, double _duration) 
:GWave(_framerate,0.0,_duration)
{
    
}

GUUNoise::~GUUNoise()
{

}

void GUUNoise::Generate(double _amplitude) 
{
    //GWave result(_framerate,0.0,_duration);
    gsl_rng * r = gsl_rng_alloc (gsl_rng_knuthran2002);
    gsl_rng_set(r, 314159265 );
    for (unsigned long long i=0; i < Count() ; i++)
    {
        double noise=gsl_rng_uniform(r) ;
        gsl_vector_set( values , i , _amplitude * (1.0 - 2. * noise) ) ;
    }
    gsl_rng_free(r);
}
