/*
 * GWave.cpp
 *
 *  Created on: Mar 3, 2021
 *      Author: RajaS
 */
#include <cmath>
#include <iostream>
#include <fstream>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics_double.h>


#include "GWave.h"

bool GWave::debug(true) ;
GWave::GWave()
: framerate(0) , start(0.0) , duration(0.0) , values(NULL)
{

}

GWave::GWave(unsigned long long _framerate , double _start, double _duration)
: framerate(_framerate) , start(_start) , duration(_duration) , values(NULL)
{
	int count = duration * framerate ;
	period = 1.0/(double)framerate ;
	values = gsl_vector_calloc (count);
}
GWave::~GWave()
{
	if (values) gsl_vector_free(values);
}

GWave GWave::Clone()
{
	GWave result (framerate,start,duration) ;
	gsl_vector_memcpy(result.values,values) ;
	return result ;
}

int GWave::Index(double _time)
{
	return (int)ApproxIndex(_time) ;
}

double GWave::ApproxIndex(double _time)
{
     return (_time - start)	/ period ;
}

void GWave::Set(double _time, double _value)
{
	int idx=Index(_time) ;
	gsl_vector_set(values, idx, _value) ;
}
void GWave::Set(unsigned long long _idx, double _value) 
{
	gsl_vector_set(values,_idx, _value) ;
}

double GWave::Value(double _time)
{
	int idx=Index(_time) ;
	return gsl_vector_get(values,idx) ;
}

void GWave::Scale(double _scale)
{
	gsl_vector_scale(values,_scale) ;
}

void GWave::Shift(double _shift)
{
	start += _shift ;
}

void GWave::Raise(double _raise)
{
	gsl_vector_add_constant(values,_raise) ;
}

void GWave::RotateRight(double _rotate)
{
	int count = duration * framerate ;
	GWave newwave (framerate,start,duration) ;
	int rotcount = _rotate * framerate ;
	int newindex = rotcount + 1 ;
	for (int i=0; i<count; i++, newindex++)
	{

		gsl_vector_set(newwave.values,newindex,gsl_vector_get(values,i)) ;
		if (newindex > count)
		{
			newindex = 0 ;
		}
	}
	gsl_vector_free(values) ;
	values = newwave.values ;
}

void GWave::RotateLeft(double _rotate)
{
	int count = duration * framerate ;
	GWave newwave (framerate,start,duration) ;
	int rotcount = _rotate * framerate ;
	int newindex = - rotcount ;
	for (int i=0; i<count; i++, newindex++)
	{
		if (newindex < 0)
		{
			gsl_vector_set(newwave.values, count + newindex , gsl_vector_get(values,i)) ;
		}
		else
		{
			gsl_vector_set(newwave.values, newindex, gsl_vector_get(values,i));
		}
	}
	gsl_vector_free(values) ;
	values = newwave.values ;
}

void GWave::Truncate(double _duration)
{
	int count = _duration * framerate ;
	GWave newwave(framerate,start,_duration) ;
	for (int i=0; i<count; i++)
	{
		gsl_vector_set(newwave.values,i,gsl_vector_get(values,i)) ;
	}
	gsl_vector_free(values) ;
	values = newwave.values ;
}
void GWave::ZeroPad(double _newstart, double _newdur)
{
	int count = duration * framerate ;
	GWave newwave(framerate,_newstart,_newdur) ;
	int idx=newwave.Index(start) ;
	for (int i=0; i<count; i++)
	{
		gsl_vector_set(newwave.values,idx+i,gsl_vector_get(values,i)) ;
	}
	gsl_vector_free(values) ;
	values = newwave.values ;
}
void GWave::Show(std::string _filename)
{
	std::ofstream csvfile(_filename) ;
	double time = start ;
	for (int i=0; i < Count() ; i++ , time += period)
	{
		csvfile << time << " , " << gsl_vector_get(values,i) << std::endl ;
	}
    csvfile.close() ;
}
//*****************************************************************************************************
bool GWave::IsCompatible(GWave& _other)
{
     if (framerate != _other.framerate) return false;
     double intidx, realidx ;
     if (start >= _other.start)
     {
         intidx = (double)_other.Index(start) ;
         realidx = _other.ApproxIndex(start) ;
         if (fabs(intidx - realidx) > IDXEPSILON)
         {
         	return false ;
         }
     }
     else
     {
    	 return _other.IsCompatible(*this) ;
     }
     return true ;
}

void GWave::Add(GWave& _other)
{
	if (IsCompatible(_other))
	{
		gsl_vector_add(values,_other.values) ;
	}
	else gsl_error("Incompatible waves for Addition", __FILE__ , __LINE__ , GSL_EINVAL) ;
}

void GWave::Sub(GWave& _other)
{
	if (IsCompatible(_other))
	{
		gsl_vector_sub(values,_other.values) ;
	}
	else gsl_error("Incompatible waves for Addition", __FILE__ , __LINE__ , GSL_EINVAL) ;
}
void GWave::Mul(GWave& _other)
{
	if (IsCompatible(_other))
	{
		gsl_vector_mul(values,_other.values) ;
	}
	else gsl_error("Incompatible waves for Addition", __FILE__ , __LINE__ , GSL_EINVAL) ;
}
void GWave::Div(GWave& _other)
{
	if (IsCompatible(_other))
	{
		gsl_vector_div(values,_other.values) ;
	}
	else gsl_error("Incompatible waves for Addition", __FILE__ , __LINE__ , GSL_EINVAL) ;
}
//*********************************Properties************************************************


double GWave::Norm(int _power) 
{
    if (_power < 2) return INFINITY ;
	double power = (double)_power ;
	double sum = 0.0 ;
	for (int i=0; i < values->size ; i++)
	{
		sum += pow(fabs(values->data[i]) , power) ;
	}
	double norm = pow(sum, 1.0 / power );
	return norm ;
}
//*********************************************************************************
double GWave::Correlation(GWave& _other)
{
	double result = gsl_stats_correlation( values->data , values->stride, _other.values->data, _other.values->stride, values->size);
	return result ;
}

double GWave::SerialCorrelation(double _shift) 
{
	int shift_count = _shift * framerate ;
	gsl_vector_const_view truncated = gsl_vector_const_subvector(values , 0 , values->size - shift_count ) ;
	gsl_vector_const_view shifted = gsl_vector_const_subvector(values , shift_count , values->size - shift_count ) ;
	double result = gsl_stats_correlation(truncated.vector.data, values->stride , shifted.vector.data, values->stride , values->size );
	return result ;
}
//*********************************************************************************
GWave GWave::Segment(double _start, double _duration, bool _rebase ) 
{
     int idx = Index(_start) ;
	 int count = _duration * framerate ;

	GWave result(framerate, _start, _duration );
	int target=0;


	for (int i=idx; i < Count() && target < count ; i++, target++ )
	{		

		gsl_vector_set(result.values, target , gsl_vector_get( values, i) );

	}
	if (_rebase)
	{
		result.start = 0.0 ;
	}
	 return result ;
}

//*****************************************Class Methods****************************************
