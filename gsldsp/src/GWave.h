/*
 * GWave.h
 *
 *  Created on: Mar 3, 2021
 *      Author: RajaS
 */
/// \brief GWave - Discrete samples of a signal
///
/// GWave represents a set of discrete samples of a signal. A GWave has a starting time
/// a duration and a sampling rate alias framerate.
///
/// Implementation
///
/// Gnu Scientific Library is used wherever possible for implementing the GWave.
///
#ifndef GSLDSP_SRC_GWAVE_H_
#define GSLDSP_SRC_GWAVE_H_

#define _USE_MATH_DEFINES
#include <cmath>

#include <string>
#include <gsl/gsl_vector.h>

class GSpectrum ;
class GWaveView ;

class GWave {
public:
	GWave(unsigned long long _framerate , double _start , double _duration );
	virtual ~GWave();
	GWave(GSpectrum &_spectrum) ;

	GWave Clone() ;
	int Index(double _time) ;
	double ApproxIndex(double _time) ;
	void Set(double _time, double _value) ;
	void Set(unsigned long long _idx, double _value) ;
	
	double Value(double _time) ;
	double Value(int idx) { return gsl_vector_get(values,idx) ;}
	int Count() { return duration * framerate ; }
	double Start() { return start ; }
	double Period() { return period ; }
	unsigned long long Framerate() { return framerate ; }
	void Scale(double _scale) ;
	void Shift(double _shift) ;
	void Raise(double _raise) ;

	void RotateRight(double _rotate) ;
	void RotateLeft(double _rotate) ;

	void Truncate(double _duration) ;
	void ZeroPad(double _newstart, double _newdur) ;

	void Add(GWave& _other) ;
	void Sub(GWave& _other) ;
	void Mul(GWave& _other) ;
	void Div(GWave& _other) ;

    double Norm(int _power=2) ;

	double Correlation(GWave& _other) ;
	double SerialCorrelation(double _shift) ;
	
	GWave Segment(double _start, double _duration, bool _rebase = true ) ;

	/// Show(std::string _filename) generates a comma separated file of time and sample pairs
	void Show(std::string _filename) ;
	
	static GWave Load(std::string _filename) ;
	
	static bool debug ;
protected:
	const double IDXEPSILON = 0.01 ;
	const double EPSILON = 0.0001 ;
	GWave() ;
	bool IsCompatible(GWave& _other) ;

	double start;
	double duration;
	double period ;
	unsigned long long framerate ;

	gsl_vector *values ;
	friend class GSpectrum ;
	friend class GWaveView ;
};

class GWaveView : public GWave 
{
public:
    GWaveView(GWave& _wave, double _start = 0.0 , double _duration=1.0) ;
	virtual ~GWaveView() ;
private:
	gsl_vector_view view ;
    GWaveView() ;
} ;

#endif /* GSLDSP_SRC_GWAVE_H_ */
