#include <cmath>
#include <iostream>
#include <fstream>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics_double.h>

#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_fft_complex.h>

#include "GWave.h"
#include "GSpectrum.h"

using namespace std ;

GWave::GWave(GSpectrum &_spectrum)
{
   	start = 0.0 ;
	duration = 1.0 ;
    framerate = _spectrum.complex->size ;
    period = 1.0 / framerate ;

    gsl_fft_halfcomplex_wavetable* wavetable = gsl_fft_halfcomplex_wavetable_alloc ( _spectrum.complex->size );
    gsl_fft_real_workspace* workspace = gsl_fft_real_workspace_alloc ( _spectrum.complex->size );
    cout << "Size is " << _spectrum.complex->size << " and " << _spectrum.real->size << endl ;
	//values = gsl_vector_alloc(2 * _spectrum.complex->size );
    values = gsl_vector_alloc( 2 * _spectrum.complex->size );
    cout << "Allocated vector" << endl ;

    gsl_fft_halfcomplex_unpack(_spectrum.complex->data , values->data , 1 , _spectrum.complex->size );
    cout << "Unpacked half complex data" << endl ;
    gsl_fft_halfcomplex_inverse (values->data, 1, _spectrum.complex->size , 
        const_cast<gsl_fft_halfcomplex_wavetable *>(wavetable) , 
        const_cast<gsl_fft_real_workspace *>(workspace) ) ;
    cout << "Inverse transform done" << endl ;

    gsl_fft_halfcomplex_wavetable_free (wavetable);
    gsl_fft_real_workspace_free (workspace);

} 