#include "GWave.h"

GWaveView::GWaveView(GWave& _wave, double _start , double _duration)
: GWave() 
{
   GWave::framerate = _wave.framerate ;
   GWave::start = _start ;
   GWave::duration = _duration ;
   GWave::values = nullptr ;
   unsigned long long idx = _wave.Index( _start );
   int samplecount = _duration / _wave.Period() ;
   view = gsl_vector_subvector(_wave.values,idx,samplecount);
}
GWaveView::~GWaveView()
{

}

