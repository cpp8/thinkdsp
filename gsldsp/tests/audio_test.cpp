#include <iostream>
#include "GAudio.h"

int load_test(int argc, char **argv)
{
    GAudio audio;
    audio.Load(argv[1]);
    double dur = audio.Duration() ;
    if (argc > 2)
    {
        audio.Save(argv[2],dur/2.0, dur/10.0) ;
    }
}

int main(int argc, char **argv)
{
    if (argc > 1)
    {
        load_test(argc, argv);
    }
}