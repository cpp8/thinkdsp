#include <iostream>
#include "GSpectrum.h"
#include "GNoise.h"

void noise_test()
{
    GUUNoise uunoise(1024);
    uunoise.Generate(1.0);
    uunoise.Show("uunoise.csv");
    GSpectrum uunoises(uunoise);
    uunoises.ShowPower("uunoise_power.csv");
    
    GGaussian gaussian(1024);
    gaussian.Generate(1.0,3.0) ;
    gaussian.Show("gnoise.csv") ;
    GSpectrum gaussians(gaussian) ;
    gaussians.ShowPower("gnoise_power.csv");

    GBrownian brownian(1024);
    brownian.Generate(2.0);
    brownian.Show("brownian.csv");
    GSpectrum brownians(brownian);
    brownians.ShowPower("brownian_power.csv");
}

int main(int argc, char **argv)
{
    noise_test();
}