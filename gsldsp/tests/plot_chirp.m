sig1 = csvread("lchirp.csv");
x = sig1(:,1);
y = sig1(:,2);
subplot(2,1,1)
plot(x,y)
title("Linear Chirp")
sig2 = csvread("echirp.csv");
x = sig2(:,1);
y = sig2(:,2);
subplot(2,1,2)
plot(x,y)
title("Exponential Chirp")
figure

sig1 = csvread("lchirpseg.csv");
x = sig1(:,1);
y = sig1(:,2);
subplot(2,1,1)
plot(x,y)
title("Segment of Linear Chirp")
sig2 = csvread("echirpseg.csv");
x = sig2(:,1);
y = sig2(:,2);
subplot(2,1,2)
plot(x,y)
title("Segment of Exponential Chirp")

