csine1 = csvread("csinewave.csv");
x = csine1(:,1);
y1 = csine1(:,2);
y2 = csine1(:,3);

subplot(2,1,1)
stem(x,y1);
title("Real")
subplot(2,1,2)
stem(x,y2);
title("Imag")
figure;
cexp1 = csvread("cexpwave.csv");
ex=cexp1(:,1);
ey1=cexp1(:,2);
ey2=cexp1(:,3);
subplot(2,1,1)
stem(ex,ey1)
title("Real")
subplot(2,1,2)
stem(ex,ey2)
