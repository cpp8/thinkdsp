xyvals=csvread("uunoise.csv");
xvals = xyvals(:,1);
yvals = xyvals(:,2);
subplot(2,1,1);
plot(xvals,yvals)
title("UU Noise")

subplot(2,1,2)
xyvals=csvread("gnoise.csv");
xvals = xyvals(:,1);
yvals = xyvals(:,2);
plot(xvals,yvals)
title("Gaussian Noise")