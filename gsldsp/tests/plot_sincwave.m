wave1=csvread("sincwave.csv");
x=wave1(:,1);
y=wave1(:,2);
subplot(2,1,1)
stem(x,y)
title("Sinc Wave")
subplot(2,1,2)
wave2=csvread("sincwave2.csv");
x2=wave2(:,1);
y2=wave2(:,2);
stem(x2,y2)
title("Sinc Wave 2 ")
grid on