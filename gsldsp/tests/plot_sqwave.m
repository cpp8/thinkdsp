sqwave1=csvread("sq1wave.csv")
x=sqwave1(:,1)
y=sqwave1(:,2)
subplot(2,1,1)
stem(x,y)
title("Square Wave")
subplot(2,1,2)
sq2=csvread("sq2wave.csv");
sq2x=sq2(:,1);
sq2y=sq2(:,2);
stem(sq2x,sq2y);
title("Square Wave Phase shifted")
