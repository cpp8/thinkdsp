trwave1=csvread("trwavefft.csv");
x=trwave1(:,1);
y=trwave1(:,2);
plot(x,y)
title("Triangle Wave")

figure
trwave1=csvread("trwave.csv");
x=trwave1(:,1);
y=trwave1(:,2);
subplot(2,1,1)
plot(x,y)
title("Triangle Wave - Original")

trwave1=csvread("trinvwave.csv");
x=trwave1(:,1);
y=trwave1(:,2);
subplot(2,1,2)
plot(x,y)
title("Triangle Wave - Reconstructed")