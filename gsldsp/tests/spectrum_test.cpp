#include <iostream>

#include "GWave.h"
#include "GSpectrum.h"
#include "GSinusoid.h"
#include "GTriangle.h"
#include "GLChirp.h"
#include "GEChirp.h"

using namespace std ;


int chirpfft()
{
    GLChirp chirp(11025, 0.0 , 1.0);
    chirp.Generate(1.0, 220.0 , 440.0 );
    GSpectrum spectrum(chirp) ;
    spectrum.Show("chirpfft.csv");

    GEChirp echirp(11025, 0.0 , 1.0);
    echirp.Generate(1.0, 220.0 , 440.0 );
    GSpectrum espectrum(echirp) ;
    spectrum.Show("echirpfft.csv");
}
int trfft()
{
    GTriangle triangle(8.0);
    GWave trwave=triangle.Generate(20000,0.0,1.0);
    trwave.Show("trwave.csv");
    GSpectrum spectrum(trwave) ;
    spectrum.Show("trwavefft.csv") ;

    GWave trinv( spectrum ) ;
    cout << "Performed inverse transformation" << endl ;
    trinv.Show("trinvwave.csv") ;
}
int sinefft()
{
    GSinusoid sine(2,2.0,0.0) ;
    GWave sinewave = sine.Generate(128,0.0,2.0);
    GSinusoid sine2(7,2.0,0.0) ;
    GWave sinewave2=sine2.Generate(128,0.0,2.0) ;
    sinewave.Add(sinewave2) ;

    GSinusoid sine3(19,1.0,0.0) ;
    GWave sinewave3=sine3.Generate(128,0.0,2.0) ;
    sinewave.Add(sinewave3) ;

    GSpectrum spectrum(sinewave) ;
    spectrum.Show("sinefft.csv") ;
}

int main(int argc, char **argv)
{
trfft();
}