#include <iostream>

#include "GWave.h"
#include "GSinc.h"
#include "GSinusoid.h"
#include "GSquare.h"
#include "GTestSignals.h"
#include "GComplexSinusoid.h"
#include "GComplexExponential.h"
#include "GLChirp.h"
#include "GEChirp.h"

using namespace std ;
	
int sinc_test()
{
	GSinc signal(10, 2.0, 0.0 ) ;
	GWave sincwave = signal.Generate(1024,0.0, 20.0) ;
	sincwave.Show("sincwave.csv") ;
	GSinc signal2(10, 2.0, M_PI );
	GWave sincwave2 = signal2.Generate(512,-12.,24);
	sincwave2.Show("sincwave2.csv");
}

int pulse_test()
{
	GWave pulse = GTestSignals::Pulse(128, 1.0 , 0.0, 3.0 , 1.0, 1.0) ;
	pulse.Show("pulse.csv");
	pulse.Raise(0.5) ;
	pulse.Show("raised.csv") ;
}

int corr_test()
{
	GSinusoid sine1(10 , 1.0 ) ;
	GWave wave1 = sine1.Generate(128);
	GSinusoid sine2(10 , 1.0, M_PI) ;
	GWave wave2 = sine2.Generate(128) ;
	double corr = wave1.Correlation(wave2) ;
	double corr2 = wave2.Correlation(wave1) ;
	cout << "Correlation coefficients " << corr << " " << corr2 << endl ;

	double sercorr = wave1.SerialCorrelation( 0.5 ) ;
	cout << "Serial Correlation with shift 0.1 " << sercorr << endl ;
}
int square_test()
{
	GSquare sq1 (2,1.5) ;
	GWave sq1wave = sq1.Generate(128,0.0,2.0) ;
	sq1wave.Show("sq1wave.csv") ;

	GSquare sq2( 2 , 1.5 , 0.3 ) ;
	GWave sq2wave = sq2.Generate(128,0.0,2.0) ;
	sq2wave.Show("sq2wave.csv");

}
int complex_test()
{

	GComplexSinusoid sinusoid(8,3.0) ;
	GComplexWave sinewave = sinusoid.Generate(256,0.0,3.0) ;
	sinewave.Show("csinewave.csv") ;

	GComplexExponential exponential(8,0.3) ;
	GComplexWave expwave = exponential.Generate(256,0.0,3.0) ;
	expwave.Show("cexpwave.csv") ;

}

int chirp_test()
{
	GLChirp chirp (10240,0.0,5.0) ;
	chirp.Generate(1.5, 2.0, 16.0 );
	chirp.Show("lchirp.cst") ;

	GWave seg1 = chirp.Segment(0.0,0.5) ;
	seg1.Show("lchirp1.csv") ;
	GWave seg2 = chirp.Segment(2.5,0.5);
	seg2.Show("lchirp2.csv") ;
	GWave seg3 = chirp.Segment(4.5,0.5) ;
	seg3.Show("lchirp3.csv") ;

	GEChirp echirp (10240,0.0,5.0) ;
	echirp.Generate(1.5, 2 , 16.0 );
	echirp.Show("echirp.csv") ;

	GWave eseg1 = echirp.Segment(0.0,0.5) ;
	eseg1.Show("echirp1.csv") ;
	GWave eseg2 = echirp.Segment(2.5,0.5);
	eseg2.Show("echirp2.csv") ;
	GWave eseg3 = echirp.Segment(4.0,0.5) ;
	eseg3.Show("echirp3.csv") ;


}
int chirp_test2()
{
	GLChirp chirp (10240,0.0,2.0) ;
	chirp.Generate(1.5, 2.0, 16.0 );
	chirp.Show("lchirp.csv") ;

	GWave seg1 = chirp.Segment(0.0,0.5) ;
	seg1.Show("lchirpseg.csv") ;

	GEChirp echirp (10240,0.0,2.0) ;
	echirp.Generate(1.5, 2.0 , 16.0 );
	echirp.Show("echirp.csv") ;
	GWave seg2 = echirp.Segment(0.0,0.5) ;
	seg2.Show("echirpseg.csv") ;

}
int main(int argc, char **argv)
{
    chirp_test2() ;
}